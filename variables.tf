variable "aaaa_record_name" {
  description = "The name of the AAAA record"
}

variable "zone_name" {
  description = "The zone name"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "ttl" {
  description = "The ttl value"
}

variable "records" {
  description = "The records IP addresses"
}

variable "tags" {
  description = "The tags to apply to the resource"
  type        = "map"
}
